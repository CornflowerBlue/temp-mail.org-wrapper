using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

public class TempMail : IDisposable
{
    private const string BaseUrl = "http://temp-mail.org/";
    private const string RefreshUrl = "http://temp-mail.org/option/refresh";

    private readonly WebClientEx _client;

    public string EmailAddress { get; private set; }

    public TempMail(string email)
    {
        _client = new WebClientEx {Proxy = null};
        _client.CookieContainer.Add(new Uri(BaseUrl), new Cookie("mail", email));
            
        EmailAddress = email;
    }

    public async Task<bool> VerifyEmail()
    {
        return string.Equals(EmailAddress, await GetEmailAddress(), StringComparison.CurrentCultureIgnoreCase);
    }

    public async Task<Email[]> GetEmails()
    {
        var data = await _client.DownloadStringTaskAsync(RefreshUrl);
        var matches = Regex.Matches(data, "<td>(.*?) &lt;(.*?)&gt;<\\/td>\\s*<td><a href=\"(.*?)\" title=\"[.\\s\\S]*?\" class=\"title-subject\">(.*?)<\\/a><\\/td>");

        return (from Match m in matches select new Email(m.Groups[2].Value, m.Groups[4].Value, m.Groups[3].Value)).ToArray();
    }

    public async Task<string> ReadEmail(Email email)
    {
        return Regex.Match(await _client.DownloadStringTaskAsync(email.DownloadUrl), "<div class=\\\"pm-text\\\">([\\s\\S.]*?)</div>").Groups[1].Value;
    }

    private async Task<string> GetEmailAddress()
    {
        var data = await _client.DownloadStringTaskAsync(BaseUrl);
        return Regex.Match(data, "\"Your temporary Email address\" class=\"mail opentip\" value=\"(.*?)\"").Groups[1].Value;
    }

    public override string ToString()
    {
        return EmailAddress;
    }

    public void Dispose()
    {
        _client.Dispose();
    }

    public static async Task<TempMail> GetNewEmail()
    {
        var data = await new WebClientEx().DownloadStringTaskAsync(BaseUrl);
        var email = Regex.Match(data, "\"Your temporary Email address\" class=\"mail opentip\" value=\"(.*?)\"").Groups[1].Value;

        return new TempMail(email);
    }

    public static TempMail GetNewEmail(string domain, int length = 12)
    {
        if (!domain.StartsWith("@"))
            throw new InvalidDataException("Invalid domain. Missing @");
        if(!domain.Contains("."))
            throw new InvalidDataException("Invalid domain. Missing TLD");

        return new TempMail(RandomUsername(length) + domain);
    }

    private static string RandomUsername(int length)
    {
        var chars = "qwertyuiopasdfghjklzxcvbnm1234567890".ToCharArray();
        var sb = new StringBuilder();
        var rand = new Random();
        for (int i = 0; i < length; i++)
            sb.Append(chars[rand.Next(0, chars.Length)]);

        return sb.ToString();
    }
}

public sealed class Email
{
    public string Sender { get; private set; }
    public string Subject { get; private set; }

    internal string DownloadUrl { get; private set; }

    internal Email(string sender, string subject, string downloadUrl)
    {
        Sender = sender;
        Subject = subject;
        DownloadUrl = downloadUrl;
    }
}

public class WebClientEx : WebClient
{
    public CookieContainer CookieContainer = new CookieContainer();

    protected override WebRequest GetWebRequest(Uri address)
    {
        var request = base.GetWebRequest(address);
        if (request is HttpWebRequest)
        {
            (request as HttpWebRequest).CookieContainer = CookieContainer;
        }
        return request;
    }
}
