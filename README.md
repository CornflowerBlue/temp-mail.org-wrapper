# Example use: #

```
#!c#

public static async void Example()
{
    // Example using a generated email
    using (var t = await TempMail.GetNewEmail())
    {
        var emails = await t.GetEmails();
        Console.WriteLine(t.EmailAddress); // This will print the email address
        foreach (var email in emails)
        {
            Console.WriteLine(await t.ReadEmail(email)); // This will print every email
        }
    }


    // Example using custom email
    using (var t = new TempMail("Example@landmail.co"))
    {
        if (!await t.VerifyEmail())
        {
            Console.WriteLine("Email is not valid!");
            return;
        }

        var emails = await t.GetEmails();
        Console.WriteLine(t.EmailAddress); // This will print the email address
        foreach (var email in emails)
        {
            Console.WriteLine(await t.ReadEmail(email)); // This will print every email
        }
    }
}
```